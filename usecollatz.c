#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
	char n[3];
	pid_t pid;
	printf("Please enter a number [1-100]:");
	scanf("%s", n);
	if((atoi(n)) > 100 || (atoi(n)) <1 ){
		printf("It isn't hard to follow simple instructions\n");
		return 0;
	}
	printf("You entered: %s\n", n);
	pid = fork();
	wait();
	if(pid == 0){
		execl("/home/astoer1/Desktop/lab4/collatz", "./collatz", n, NULL);
	}
	return 0;
}
