#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
	printf("Before Forking \n");
	fork();
	printf("after forking \n");
	return 0;
}
