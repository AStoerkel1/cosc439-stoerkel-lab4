all: small.c collatz.c usecollatz.c
	gcc small.c -Wno-implicit -o small
	gcc collatz.c -Wno-implicit -o collatz
	gcc usecollatz.c -Wno-implicit -o usecollatz

usecollatz: usecollatz.c collatz
	gcc usecollatz.c -Wno-implicit -o usecollatz 
	
collatz: collatz.c
	gcc collatz.c -Wno-implicit -o collatz
	
small: small.c
	gcc small.c -Wno-implicit -o small

run: small collatz usecollatz
	./small
	./collatz 5
	./usecollatz
clean:
	rm small collatz usecollatz
