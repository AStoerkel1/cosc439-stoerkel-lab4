#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>

/*this program still needs to accept multiple digit arguments 
look at getopt() for a better way*/
int main(int c, char *argv[]) {
	
	pid_t pid;
	int iargv = 0;
	int option = getopt(c, argv, "");
	if(c>2){
		printf("please only enter one argument\n");
		return 0;
	}
	for(; optind < c; optind++) {
		iargv = atoi(argv[optind]);
	}
	printf("START\n");
	if(iargv<1){
		printf("please enter a positive number \nEND\n");
	}else{
		pid = fork();
		wait();
		if (pid == 0){
			collatz(iargv);
		}else{
			printf("END\n");
		}
	}
	return 0;
}

int collatz(int n){
	printf("%d\n",n);	
	if(n==1){
		return 0;
	}
	else if (n%2 ==0){
		return collatz(n/2);
	}
	else{
		return collatz(3*n + 1);
	}
}
